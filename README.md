# CSS-Basic-HomeTask
## Homework EPAM Upskill

Discrepancies fixed:
1. Whitespaces deleted
2. HTML markup passed validation
3. Classes used for all elements related to markup
4. Comments deleted
5. Extra empty div in .lorem deleted
6. h1 margin-top deleted
7. Menu icon aligned without flex and float
8. .top-panel not sticking out to the right
9. Excessive paddings, margins, and font styles deleted
10. Color codes brought to one standard
11. No id`s anymore
12. border-radius set in %
13. Class names changed to representative (to be brought to BEM system in the next task)
14. <input type="button"> changed to <button>
15. font-family order changed
16. Negative margins removed
17. 115 px changed to 115px
18. Margins set as margin: x x x x instead of margin-top, etc.
19. Excessive files removed from the repository
